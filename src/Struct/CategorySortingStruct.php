<?php
namespace Memo\CustomCmsBlocks\Struct;
use Shopware\Core\Framework\Struct\Struct;

class CategorySortingStruct extends Struct
{
    public string $id;

    public ?string $afterCategoryId;

    public ?string $parentId;

    public ?array $children = null;

    public function __construct(string $id, ?string $afterCategoryId, ?string $parentId)
    {
        $this->id = $id;
        $this->afterCategoryId = $afterCategoryId;
        $this->parentId = $parentId;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getAfterCategoryId(): ?string
    {
        return $this->afterCategoryId;
    }

    public function setAfterCategoryId(?string $afterCategoryId): void
    {
        $this->afterCategoryId = $afterCategoryId;
    }

    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    public function setParentId(?string $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function getChildren(): ?array
    {
        return $this->children;
    }

    public function setChildren(?array $children): void
    {
        $this->children = $children;
    }
}
