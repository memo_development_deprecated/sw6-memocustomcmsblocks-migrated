import MemoCollapsePlugin from './memo-collapse/memo-collapse.plugin';

const PluginManager = window.PluginManager;
PluginManager.register('MemoCollapsePlugin', MemoCollapsePlugin, '[data-memo-collapse]');
