import Plugin from 'src/plugin-system/plugin.class';

export default class MemoCollapsePlugin extends Plugin {
    init() {
        const buttons = this.el.querySelectorAll('[data-toggle="collapse"]');

        buttons.forEach((button) => {
            button.addEventListener('click', this.onClick.bind(this));
        });
    }

    onClick(event) {
        event.preventDefault();
        const target = event.currentTarget;
        const selector = target.dataset.target;

        document.querySelectorAll(selector).forEach((element) => {
            element.classList.contains('show');

            if (element.classList.contains('show')) {
                element.classList.remove('show');
                target.setAttribute('aria-expanded', 'false');
            } else {
                element.classList.add('show');
                target.setAttribute('aria-expanded', 'true');
            }
        });
    }
}
