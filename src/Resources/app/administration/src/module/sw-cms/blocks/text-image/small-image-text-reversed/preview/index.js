import template from './sw-cms-preview-small-image-text-reversed.html.twig';
import './sw-cms-preview-small-image-text-reversed.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-small-image-text-reversed', {
    template
});
