import template from './sw-cms-block-small-top-images-text.html.twig';
import './sw-cms-block-small-top-images-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-small-top-images-text', {
    template
});
