import template from './sw-cms-preview-memo-category-navigation.html.twig';
import './sw-cms-preview-memo-category-navigation.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-memo-category-navigation', {
    template
});
