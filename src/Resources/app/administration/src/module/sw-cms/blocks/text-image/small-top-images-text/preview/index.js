import template from './sw-cms-preview-small-top-images-text.html.twig';
import './sw-cms-preview-small-top-images-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-small-top-images-text', {
    template
});
