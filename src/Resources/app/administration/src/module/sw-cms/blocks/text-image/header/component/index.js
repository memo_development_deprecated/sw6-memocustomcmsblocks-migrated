import template from './sw-cms-block-header.html.twig';
import './sw-cms-block-header.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-header', {
    template
});
