import template from './sw-cms-block-two-small-top-images-text.html.twig';
import './sw-cms-block-two-small-top-images-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-two-small-top-images-text', {
    template
});
