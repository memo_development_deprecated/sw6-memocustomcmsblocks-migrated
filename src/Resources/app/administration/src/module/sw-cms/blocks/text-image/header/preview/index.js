import template from './sw-cms-preview-header.html.twig';
import './sw-cms-preview-header.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-header', {
    template
});
