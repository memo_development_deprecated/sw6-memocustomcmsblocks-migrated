import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'memo-category-navigation',
    label: 'sw-cms.blocks.sidebar.memoCategoryNavigation.label',
    category: 'sidebar',
    component: 'sw-cms-block-memo-category-navigation',
    previewComponent: 'sw-cms-preview-memo-category-navigation',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        'categoryNavigation': {
            type: 'memo-category-navigation',
        },
    }
});
