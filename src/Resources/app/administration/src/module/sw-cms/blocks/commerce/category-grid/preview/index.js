import template from './sw-cms-preview-category-grid.html.twig';
import './sw-cms-preview-category-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-category-grid', {
    template
});
