import template from './sw-cms-block-memo-category-navigation.html.twig';
import './sw-cms-block-memo-category-navigation.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-memo-category-navigation', {
    template
});
