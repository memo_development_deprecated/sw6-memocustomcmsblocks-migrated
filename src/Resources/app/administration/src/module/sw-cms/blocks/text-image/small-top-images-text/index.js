import './component';
import './preview';


Shopware.Service('cmsService').registerCmsBlock({
    name: 'small-top-images-text',
    label: 'sw-cms.blocks.textImage.smallTopImagesText.label',
    category: 'text-image',
    component: 'sw-cms-block-small-top-images-text',
    previewComponent: 'sw-cms-preview-small-top-images-text',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        title: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        },
        image: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        title2: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description2: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        },
        image2: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        title3: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description3: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        },
        image3: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
    }
});
