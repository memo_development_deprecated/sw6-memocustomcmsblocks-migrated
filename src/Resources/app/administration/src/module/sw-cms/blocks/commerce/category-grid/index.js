import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'category-grid',
    label: 'sw-cms.blocks.commerce.categoryGrid.label',
    category: 'commerce',
    component: 'sw-cms-block-category-grid',
    previewComponent: 'sw-cms-preview-category-grid',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        'categoryGrid': {
            type: 'category-grid',
        },
    }
});
