import template from './sw-cms-preview-two-small-top-images-text.html.twig';
import './sw-cms-preview-two-small-top-images-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-two-small-top-images-text', {
    template
});
