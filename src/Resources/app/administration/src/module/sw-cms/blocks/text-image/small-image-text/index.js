import './component';
import './preview';


Shopware.Service('cmsService').registerCmsBlock({
    name: 'small-image-text',
    label: 'sw-cms.blocks.textImage.smallImageText.label',
    category: 'text-image',
    component: 'sw-cms-block-small-image-text',
    previewComponent: 'sw-cms-preview-small-image-text',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        image: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        title: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        },
    }
});
