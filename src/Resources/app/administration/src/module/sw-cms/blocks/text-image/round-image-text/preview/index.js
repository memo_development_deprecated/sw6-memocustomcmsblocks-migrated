import template from './sw-cms-preview-round-image-text.html.twig';
import './sw-cms-preview-round-image-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-round-image-text', {
    template
});
