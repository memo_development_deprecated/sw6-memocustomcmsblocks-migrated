import './component';
import './preview';


Shopware.Service('cmsService').registerCmsBlock({
    name: 'two-small-top-images-text',
    label: 'sw-cms.blocks.textImage.twoSmallTopImagesText.label',
    category: 'text-image',
    component: 'sw-cms-block-two-small-top-images-text',
    previewComponent: 'sw-cms-preview-two-small-top-images-text',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        image: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        title: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        },
        image2: {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        title2: {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor'
                    }
                },
            }
        },
        description2: {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                    }
                }
            }
        }
    }
});
