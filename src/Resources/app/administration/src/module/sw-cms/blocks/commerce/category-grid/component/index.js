import template from './sw-cms-block-category-grid.html.twig';
import './sw-cms-block-category-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-category-grid', {
    template
});
