import template from './sw-cms-block-small-image-text.html.twig';
import './sw-cms-block-small-image-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-small-image-text', {
    template
});
