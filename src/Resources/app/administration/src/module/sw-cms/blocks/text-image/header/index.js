import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'header',
    label: 'sw-cms.blocks.textImage.header.label',
    category: 'text-image',
    component: 'sw-cms-block-header',
    previewComponent: 'sw-cms-preview-header',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        'backgroundImage': {
            type: 'image',
            default: {
                config: {
                    displayMode: {source: 'static', value: 'cover'}
                },
                data: {
                    media: {
                        url: '/administration/static/img/cms/preview_mountain_large.jpg'
                    }
                }
            }
        },
        'title': {
            type: 'textline',
            default: {
                config: {
                    content: {
                        value: 'Überschrift'
                    }
                },
            }
        },
        'description': {
            type: 'textarea',
            default: {
                config: {
                    content: {
                        value: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.'
                    }
                }
            }
        },
        'button': {
            type: 'button',
            default: {
                config: {
                    content: {
                        value: 'Mehr erfahren'
                    }
                }
            }
        },
    }
});
