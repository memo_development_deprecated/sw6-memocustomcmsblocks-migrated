import template from './sw-cms-block-small-image-text-reversed.html.twig';
import './sw-cms-block-small-image-text-reversed.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-small-image-text-reversed', {
    template
});
