import template from './sw-cms-preview-small-image-text.html.twig';
import './sw-cms-preview-small-image-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-small-image-text', {
    template
});
