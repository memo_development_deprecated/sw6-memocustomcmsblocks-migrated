import template from './sw-cms-block-round-image-text.html.twig';
import './sw-cms-block-round-image-text.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-round-image-text', {
    template
});
