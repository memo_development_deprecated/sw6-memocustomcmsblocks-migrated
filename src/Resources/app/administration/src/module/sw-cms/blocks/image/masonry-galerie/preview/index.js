import template from './sw-cms-preview-masonry-galerie.html.twig';
import './sw-cms-preview-masonry-galerie.scss';

Shopware.Component.register('sw-cms-preview-masonry-galerie', {
    template
});
