import template from './sw-cms-block-masonry-galerie.html.twig';
import './sw-cms-block-masonry-galerie.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-masonry-galerie', {
    template
});
