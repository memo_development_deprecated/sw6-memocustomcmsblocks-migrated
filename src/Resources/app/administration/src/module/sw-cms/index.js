import './elements';
import './blocks';

import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';
