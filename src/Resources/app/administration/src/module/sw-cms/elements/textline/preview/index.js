import template from './sw-cms-el-preview-textline.html.twig';
import './sw-cms-el-preview-textline.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-textline', {
    template
});
