import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'textline',
    label: 'sw-cms.elements.textline.label',
    component: 'sw-cms-el-textline',
    configComponent: 'sw-cms-el-config-textline',
    previewComponent: 'sw-cms-el-preview-textline',
    defaultConfig: {
        content: {
            source: 'static',
            value: 'Lorem Ipsum dolor sit amet'
        },
        verticalAlign: {
            source: 'static',
            value: null
        },
        headlineStyle: {
            source: 'static',
            value: null
        },
        headline: {
            source: 'static',
            value: null
        }
    }
});
