import template from './sw-cms-el-config-button.html.twig';
import './sw-cms-el-config-button.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-button', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('button');
        },

        onBlur(content) {
            // this.emitChanges(content);
        },

        onBlurLink(content) {
            // this.emitChangesLink(content);
        },

        onInput(content) {
            this.emitChanges(content);
        },

        onInputLink(content) {
            this.emitChangesLink(content);
        },

        emitChanges(content) {
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },

        emitChangesLink(content) {
            if (content !== this.element.config.link.value) {
                this.element.config.link.value = content;
                this.$emit('element-update', this.element);
            }
        }
    }
});
