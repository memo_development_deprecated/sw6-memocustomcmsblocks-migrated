import template from './sw-cms-el-preview-memo-category-navigation.html.twig';
import './sw-cms-el-preview-memo-category-navigation.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-memo-category-navigation', {
    template
});
