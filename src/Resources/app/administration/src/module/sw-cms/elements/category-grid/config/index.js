import template from './sw-cms-el-config-category-grid.html.twig';
import './sw-cms-el-config-category-grid.scss';

const { Component, Mixin } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('sw-cms-el-config-category-grid', {
    template,

    inject: ['repositoryFactory', 'syncService'],

    mixins: [
        Mixin.getByName('cms-element')
    ],

    props: {
        categoryId: {
            type: String,
            required: false,
            default: null
        },

        currentLanguageId: {
            type: String,
            required: true
        },

        allowEdit: {
            type: Boolean,
            required: false,
            default: false
        },

        allowCreate: {
            type: Boolean,
            required: false,
            default: false
        },

        allowDelete: {
            type: Boolean,
            required: false,
            default: false
        }
    },

    data() {
        return {
            loadedCategories: {},
            translationContext: 'sw-category',
            linkContext: 'sw.category.detail',
            isLoadingInitialData: true,
            loadedParentIds: []
        };
    },

    created() {
        this.createdComponent();
    },

    computed: {
        categoryRepository() {
            return this.repositoryFactory.create('category');
        },

        category() {
            return [];
        },

        categories() {
            return Object.values(this.loadedCategories);
        },

        activeCategories() {
            let tmpArray = [];

            for (let categoryId in this.element.config.content.value) {
                tmpArray.push(categoryId);
            }

            return tmpArray.join(',');
        },

        disableContextMenu() {
            if (!this.allowEdit) {
                return true;
            }

            return this.currentLanguageId !== Shopware.Context.api.systemLanguageId;
        },

        contextMenuTooltipText() {
            if (!this.allowEdit) {
                return this.$tc('sw-privileges.tooltip.warning');
            }

            return null;
        }
    },

    watch: {
        category(newVal, oldVal) {
            // load data when path is available
            if (!oldVal && this.isLoadingInitialData) {
                this.openInitialTree();
                return;
            }

            // back to index
            if (newVal === null) {
                return;
            }

            // reload after save
            if (oldVal && newVal.id === oldVal.id) {
                this.categoryRepository.get(newVal.id, Shopware.Context.api).then((newCategory) => {
                    this.loadedCategories[newCategory.id] = newCategory;
                    this.loadedCategories = { ...this.loadedCategories };
                });
            }
        },

        currentLanguageId() {
            this.openInitialTree();
        }
    },

    methods: {
        createdComponent() {
            this.initElementConfig('category-grid');

            if (!this.categoryId) {
                this.loadRootCategories().then(() => {
                    this.isLoadingInitialData = false;
                });
            }
        },

        /**
         * Loads all categories from first level.
         */
        openInitialTree() {
            this.isLoadingInitialData = true;
            this.loadedCategories = {};
            this.loadedParentIds = [];

            this.loadRootCategories()
                .then(() => {
                    if (!this.category || this.category.path === null) {
                        this.isLoadingInitialData = false;
                        return Promise.resolve();
                    }

                    const parentIds = this.category.path.split('|').filter((id) => !!id);
                    const parentPromises = [];

                    parentIds.forEach((id) => {
                        const searchCriteria = (new Criteria(1, 1))
                            .addAssociation('children');

                        searchCriteria.getAssociation('children')
                            .setLimit(500);

                        const promise = this.categoryRepository.get(id, Shopware.Context.api, searchCriteria)
                            .then((result) => {
                                this.addCategories([result, ...result.children]);
                            });
                        parentPromises.push(promise);
                    });

                    return Promise.all(parentPromises).then(() => {
                        this.isLoadingInitialData = false;
                    });
                });
        },

        /**
         * Gets called when category gets checked.
         *
         * @param category
         */
        onCheckItem(category) {
            this.emitChanges(category);
        },

        /**
         * Finds Subcategories for category when opening a category with children.
         *
         * @param parentId
         * @returns {Promise<void>|*}
         */
        onGetTreeItems(parentId) {
            if (this.loadedParentIds.includes(parentId)) {
                return Promise.resolve();
            }

            this.loadedParentIds.push(parentId);
            const categoryCriteria = new Criteria(1, 500);
            categoryCriteria.addFilter(Criteria.equals('parentId', parentId));

            return this.categoryRepository.search(categoryCriteria, Shopware.Context.api).then((children) => {
                this.addCategories(children);
            }).catch(() => {
                this.loadedParentIds = this.loadedParentIds.filter((id) => {
                    return id !== parentId;
                });
            });
        },

        /**
         * Loads the root categories.
         */
        loadRootCategories() {
            const criteria = new Criteria();
            criteria.limit = 500;
            criteria.addFilter(Criteria.equals('parentId', null));

            return this.categoryRepository.search(criteria, Shopware.Context.api).then((result) => {
                this.addCategories(result);
            });
        },

        /**
         * Adds the given categories to the list.
         *
         * @param categories
         */
        addCategories(categories) {
            categories.forEach((category) => {
                category.selected = false;

                // Item is selected.
                if (category.id in this.element.config.content.value) {
                    category.selected = true;
                }

                this.loadedCategories[category.id] = category;
            });
            this.loadedCategories = { ...this.loadedCategories };
        },

        /**
         * Gets called to save the changes.
         *
         * @param category
         */
        emitChanges(category) {
            let element = category.data;

            // Dirty Hack: This is needed because when every value from content gets removed the settings will be destroyed.
            this.element.config.content.value['secure'] = true;

            if (category.checked === true) {
                // Add element to config.
                this.element.config.content.value[element.id] = element;
            } else {
                // Remove element from config.
                delete this.element.config.content.value[element.id];
            }

            this.$emit('element-update', this.element);
        }
    }
});
