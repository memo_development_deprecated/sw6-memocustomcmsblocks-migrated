import template from './sw-cms-el-preview-category-grid.html.twig';
import './sw-cms-el-preview-category-grid.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-category-grid', {
    template
});
