import template from './sw-cms-el-memo-category-navigation.html.twig';
import './sw-cms-el-memo-category-navigation.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-memo-category-navigation', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    data() {
        return {
            editable: false,
            demoValue: ''
        };
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('memo-category-navigation');
        },

        onInput(content) {
            this.emitChanges(content);
        },

        emitChanges(content) {
            this.$emit('element-update', this.element);
        }
    }
});
