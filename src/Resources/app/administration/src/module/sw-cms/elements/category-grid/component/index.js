import template from './sw-cms-el-category-grid.html.twig';
import './sw-cms-el-category-grid.scss';

const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-category-grid', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    data() {
        return {
            editable: false,
            demoValue: ''
        };
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('category-grid');
        },

        getItemLabel(index, defaultText) {
            let content = {...this.element.config.content.value}; // Clone object.
            delete content.secure; // Remove secure key.
            let value = Object.values(content)[index];

            return value ? value.name : defaultText;
        },

        hasItem(index) {
            let content = {...this.element.config.content.value}; // Clone object.
            delete content.secure; // Remove secure key.
            let totalLength = Object.values(content).length;

            return totalLength === 0 ? true : index < totalLength;
        },

        onInput(content) {
            this.emitChanges(content);
        },

        emitChanges(content) {
            this.$emit('element-update', this.element);
        }
    }
});
