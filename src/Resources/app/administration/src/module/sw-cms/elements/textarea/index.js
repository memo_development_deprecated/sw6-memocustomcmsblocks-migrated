import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'textarea',
    label: 'sw-cms.elements.textarea.label',
    component: 'sw-cms-el-textarea',
    configComponent: 'sw-cms-el-config-textarea',
    previewComponent: 'sw-cms-el-preview-textarea',
    defaultConfig: {
        content: {
            source: 'static',
            value: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum."
        },
        verticalAlign: {
            source: 'static',
            value: null
        }
    }
});
