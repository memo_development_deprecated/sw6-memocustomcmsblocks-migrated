import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'memo-category-navigation',
    label: 'sw-cms.elements.memo-category-navigation.label',
    component: 'sw-cms-el-memo-category-navigation',
    configComponent: 'sw-cms-el-config-memo-category-navigation',
    previewComponent: 'sw-cms-el-preview-memo-category-navigation',
    defaultConfig: {
        content: {
            source: 'static',
            value: {},
        },
        maxdepth: {
            source: 'static',
            value: '3'
        },
    }
});
