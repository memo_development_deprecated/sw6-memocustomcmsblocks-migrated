import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'category-grid',
    label: 'sw-cms.elements.category-grid.label',
    component: 'sw-cms-el-category-grid',
    configComponent: 'sw-cms-el-config-category-grid',
    previewComponent: 'sw-cms-el-preview-category-grid',
    defaultConfig: {
        content: {
            source: 'static',
            value: {
                'secure': true,
            },
        },
        itemCount: {
            source: 'static',
            value: '4',
        }
    }
});
