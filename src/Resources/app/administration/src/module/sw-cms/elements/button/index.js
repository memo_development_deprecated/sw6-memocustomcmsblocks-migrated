import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'button',
    label: 'sw-cms.elements.button.label',
    component: 'sw-cms-el-button',
    configComponent: 'sw-cms-el-config-button',
    previewComponent: 'sw-cms-el-preview-button',
    defaultConfig: {
        content: {
            source: 'static',
            value: 'Mehr erfahren'
        },
        link: {
            source: 'static',
            value: 'test'
        },
        verticalAlign: {
            source: 'static',
            value: null
        }
    }
});
