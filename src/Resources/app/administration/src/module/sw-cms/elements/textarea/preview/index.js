import template from './sw-cms-el-preview-textarea.html.twig';
import './sw-cms-el-preview-textarea.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-textarea', {
    template
});
