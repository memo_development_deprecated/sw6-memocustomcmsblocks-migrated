<?php declare(strict_types=1);

namespace Memo\CustomCmsBlocks;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class MemoCustomCmsBlocks extends Plugin
{
    public function install(InstallContext $installContext): void
    {
        parent::install($installContext);

        $this->createCustomFields($installContext->getContext());
    }

    public function update(UpdateContext $updateContext): void
    {
        parent::update($updateContext);

        $this->createCustomFields($updateContext->getContext());
    }

    private function createCustomFields(Context $context)
    {
        $customFieldSets = [
            [
                'name' => 'memo_custom_cms_blocks_fields_category',
                'active' => true,
                'config' => [
                    'label' => [
                        'en-GB' => 'Custom CMS Blocks',
                        'de-DE' => 'Custom CMS Blöcke'
                    ],
                    'translated' => true,
                    'position' => 1
                ],
                'customFields' => [
                    [
                        'name' => 'memo_custom_cms_blocks_fields_category_category_grid_title',
                        'type' => CustomFieldTypes::TEXT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Title for Category Grid',
                                'de-DE' => 'Titel für Kategorie Kacheln',
                            ],
                            'componentName' => 'sw-field',
                            'customFieldPosition' => 1
                        ]
                    ],
                    [
                        'name' => 'memo_custom_cms_blocks_fields_category_category_grid_description',
                        'type' => CustomFieldTypes::HTML,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Description for Category Grid',
                                'de-DE' => 'Beschreibung für Kategorie Kacheln',
                            ],
                            'componentName' => 'sw-text-editor',
                            'customFieldPosition' => 2
                        ]
                    ],
                    [
                        'name' => 'memo_custom_cms_blocks_fields_category_category_grid_image',
                        'type' => CustomFieldTypes::MEDIA,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Image for Categorie Grid',
                                'de-DE' => 'Bild für Kategorie Kacheln',
                            ],
                            'componentName' => 'sw-media-field',
                            'customFieldPosition' => 3
                        ]
                    ],
                    [
                        'name' => 'memo_custom_cms_blocks_fields_category_category_grid_image_position',
                        'type' => CustomFieldTypes::SELECT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Imageposition for Categorie Grid',
                                'de-DE' => 'Bildposition für Kategorie Kacheln',
                            ],
                            'componentName' => 'sw-single-select',
                            'customFieldPosition' => 4,
                            'options' => [
                                [
                                    'value' => 'left bottom',
                                    'label' => [
                                        'de-DE' => 'Links Unten',
                                        'en-GB' => 'Left Bottom',
                                    ]
                                ],
                                [
                                    'value' => 'left',
                                    'label' => [
                                        'de-DE' => 'Links',
                                        'en-GB' => 'Left',
                                    ]
                                ],
                                [
                                    'value' => 'left top',
                                    'label' => [
                                        'de-DE' => 'Links Oben',
                                        'en-GB' => 'Left Top',
                                    ]
                                ],
                                [
                                    'value' => 'top',
                                    'label' => [
                                        'de-DE' => 'Oben',
                                        'en-GB' => 'Top',
                                    ]
                                ],
                                [
                                    'value' => 'right top',
                                    'label' => [
                                        'de-DE' => 'Rechts Oben',
                                        'en-GB' => 'Right Top',
                                    ]
                                ],
                                [
                                    'value' => 'right',
                                    'label' => [
                                        'de-DE' => 'Rechts',
                                        'en-GB' => 'Right',
                                    ]
                                ],
                                [
                                    'value' => 'right bottom',
                                    'label' => [
                                        'de-DE' => 'Rechts Unten',
                                        'en-GB' => 'Right Bottom',
                                    ]
                                ],
                                [
                                    'value' => 'bottom',
                                    'label' => [
                                        'de-DE' => 'Unten',
                                        'en-GB' => 'Bottom',
                                    ]
                                ],
                                [
                                    'value' => 'center center',
                                    'label' => [
                                        'de-DE' => 'Zentriert',
                                        'en-GB' => 'Centered',
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'name' => 'memo_custom_cms_blocks_fields_category_category_grid_image_fit',
                        'type' => CustomFieldTypes::SELECT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Image Fit for Categorie Grid',
                                'de-DE' => 'Bild Grösse für Kategorie Kacheln',
                            ],
                            'componentName' => 'sw-single-select',
                            'customFieldPosition' => 4,
                            'options' => [
                                [
                                    'value' => 'cover',
                                    'label' => [
                                        'de-DE' => 'Bedecken / Ausfüllen',
                                        'en-GB' => 'Cover',
                                    ]
                                ],
                                [
                                    'value' => 'contain',
                                    'label' => [
                                        'de-DE' => 'Beinhalten',
                                        'en-GB' => 'Contain',
                                    ]
                                ],
                                [
                                    'value' => 'fill',
                                    'label' => [
                                        'de-DE' => 'Strecken',
                                        'en-GB' => 'Fill',
                                    ]
                                ],
                                [
                                    'value' => 'none',
                                    'label' => [
                                        'de-DE' => 'Original',
                                        'en-GB' => 'Original',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'relations' => [
                    [
                        'entityName' => 'category'
                    ]
                ]
            ]
        ];

        $customFieldSetRepository = $this->container->get('custom_field_set.repository');
        $customFieldRepository = $this->container->get('custom_field.repository');

        foreach ($customFieldSets as $customFieldSet) {
            $criteria = (new Criteria())
                ->addFilter(
                    new EqualsFilter('name', $customFieldSet['name'])
                );

            $existingCustomFieldSets = $customFieldSetRepository
                ->search($criteria, $context);

            if ($existingCustomFieldSets->getTotal() !== 0) {
                $customFieldSetRepository->delete([['id' => $existingCustomFieldSets->first()->getId()]], $context);

                foreach ($customFieldSet['customFields'] as $key => $customFields) {
                    $criteria = (new Criteria())
                        ->addFilter(
                            new EqualsFilter('name', $customFields['name'])
                        )
                        ->addFilter(
                            new EqualsFilter('customFieldSetId', $existingCustomFieldSets->first()->getId())
                        );

                    $existingCustomField = $customFieldRepository
                        ->search($criteria, $context);

                    if ($existingCustomField->getTotal() === 0) {
                        continue;
                    }

                    $customFieldSet['customFields'][$key]['id'] = $existingCustomField->first()->getId();
                }
            }

            $customFieldSetRepository->upsert([$customFieldSet], $context);
        }
    }
}
