<?php

namespace Memo\CustomCmsBlocks\Twig;

use Doctrine\DBAL\Connection;
use Memo\CustomCmsBlocks\Struct\CategorySortingStruct;
use Shopware\Core\Content\Category\CategoryCollection;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Util\AfterSort;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class CustomCMSExtension extends AbstractExtension
{
    /**
     * @var EntityRepository
     */
    private $categoryRepository;

    /**
     * @var EntityRepository
     */
    private $mediaRepository;

    /**
     * @var array
     */
    private $categorySorting = [];

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param EntityRepository $categoryRepository
     * @param EntityRepository $mediaRepository
     */
    public function __construct(EntityRepository $categoryRepository, EntityRepository $mediaRepository, Connection $connection)
    {
        $this->categoryRepository = $categoryRepository;
        $this->mediaRepository = $mediaRepository;
        $this->connection = $connection;
        $this->context = Context::createDefaultContext();
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('load_categories', [$this, 'loadCategories']),
            new TwigFilter('load_navigation_tree', [$this, 'loadNavigationTree']),
            new TwigFilter('load_media', [$this, 'loadMedia']),
            new TwigFilter('html_entity_decode', 'html_entity_decode', ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param array $categoryIds
     * @param Context $context
     *
     * @return EntityCollection
     */
    public function loadCategories(array $categoryIds, Context $context = null)
    {
        $context = is_null($context) ? $this->context : $context;
        $criteria = (new Criteria())->addFilter(new EqualsAnyFilter('id', $categoryIds))->addAssociation('media');
        $categories = $this->categoryRepository->search($criteria, $context)->getEntities();

        return $this->sortCategories($categories);
    }

    /**
     * @param CategoryCollection $categories
     *
     * @return CategoryCollection
     */
    private function sortCategories(?CategoryCollection $categories, ?array $categorySorting = null)
    {
        if (!$categories || $categories->count() === 0) {
            return $categories;
        }

        if (is_null($categorySorting)) {
            $categorySorting = $this->getCategorySorting();
        }

        $categories->sort(function ($a, $b) use ($categorySorting) {
            $aValue = array_key_exists($a->getId(), $categorySorting) ? $categorySorting[$a->getId()] : 0;
            $bValue = array_key_exists($b->getId(), $categorySorting) ? $categorySorting[$b->getId()] : 0;

            return $aValue <=> $bValue;
        });

        foreach ($categories as $category) {
            if (is_null($category->getChildren()) || $category->getChildCount() === 0) {
                continue;
            }

            $categoryChildren = new CategoryCollection();

            foreach ($category->getChildren() as $child) {
                if ($child->getActive() === false) {
                    continue;
                }

                $categoryChildren->add($child);
            }

            $category->setChildren($this->sortCategories($categoryChildren, $categorySorting));
        }

        return $categories;
    }

    /**
     * @param array $categoryIds
     * @param int $depth
     * @param Context $context
     *
     * @return array|EntityCollection
     */
    public function loadNavigationTree(array $categoryIds, int $depth = 3, Context $context = null)
    {
        $context = is_null($context) ? $this->context : $context;
        $criteria = (new Criteria())
            ->addFilter(new EqualsAnyFilter('id', $categoryIds))
            ->addFilter(new EqualsFilter('active', 1));

        if ($depth > 0) {
            $currentChild = 'children';

            for ($i = 0; $i < $depth; $i++) {
                $criteria->addAssociation($currentChild);
                $currentChild .= '.children';
            }
        }

        $categories = $this->categoryRepository->search($criteria, $context)->getEntities();
        $categories = $this->sortCategories($categories);

        return $categories;
    }

    private function getCategorySorting()
    {
        if ($this->categorySorting) {
            return $this->categorySorting;
        }

        $sql = 'SELECT LOWER(HEX(`id`)) AS `id`, LOWER(HEX(`after_category_id`)) AS `afterCategoryId`, LOWER(HEX(`parent_id`)) AS `parentId` FROM `category`;';

        $result = $this->connection->executeQuery($sql)->fetchAll();

        $categories = [];
        $mainCategories = [];

        foreach ($result as $key => $item) {
            if (is_null($item['parentId'])) {
                $mainCategories[] = new CategorySortingStruct($item['id'], $item['afterCategoryId'], $item['parentId']);
            } else {
                $categories[$item['parentId']][] = new CategorySortingStruct($item['id'], $item['afterCategoryId'], $item['parentId']);
            }
        }

        foreach ($mainCategories as $key => $mainCategory) {
            $mainCategories[$key]->setChildren($this->sortingGetChildCategories($mainCategory->getId(), $categories));
        }

        $mainCategories = AfterSort::sort($mainCategories, 'afterCategoryId');

        $a = 0;
        $this->sortingGetValue($mainCategories, $a);

        return $this->categorySorting;
    }

    private function sortingGetChildCategories(string $mainId, array $categories): ?array
    {
        if (!array_key_exists($mainId, $categories)) {
            return null;
        }

        $children = $categories[$mainId];

        foreach($children as $key => $child) {
            $children[$key]->setChildren($this->sortingGetChildCategories($child->getId(), $categories));
        }

        return AfterSort::sort($children, 'afterCategoryId');
    }

    private function sortingGetValue(?array $categories, int &$currentValue): void
    {
        if (!$categories) {
            return;
        }

        foreach ($categories as $category) {
            $this->categorySorting[$category->getId()] = $currentValue;
            $currentValue++;

            if ($category->getChildren()) {
                $this->sortingGetValue($category->getChildren(), $currentValue);
            }
        }
    }

    /**
     * @param string $mediaId
     *
     * @return mixed|null
     */
    public function loadMedia(string $mediaId, Context $context = null)
    {
        $context = is_null($context) ? $this->context : $context;

        $criteria = (new Criteria())->addFilter(new EqualsFilter('id', $mediaId));

        return $this->mediaRepository->search($criteria, $context)->getEntities()->first();
    }
}
